# AdobePW

AdobePW is a Python library for analyzing and manipulating the 2013 Adobe breach data. This library does not include the breach data itself, or the necessary tools to fetch and parse the data. However, the schema required for compatibility with the library is included in the `build.sql` file. AdobePW was briefly discussed and demonstrated at BSides Augusta 2014 during [Stored Password Security: The Adobe Guide to Keyless Decryption](http://youtu.be/C1UqwC0SZ7c).

## Setup

#### Option 1

* Build a SQLite database using the `build.sql` file.

`sqlite3 adobepw.db < build.sql`

* Parse the original dump and import the data into the database

#### Option 2

* Find me at a conference. ;-)
* Decrypt and decompress the database.

``` bash
$ openssl aes-256-cbc -d -in adobepw.enc -out adobepw.db.tar.gz
$ tar -zxvf adobepw.db.tar.gz
```

## Usage

* Clone this repository.

`git clone https://LaNMaSteR53@bitbucket.org/LaNMaSteR53/adobepw.git`

* Optimize the database with the `clean.sh` script. This script vacuums the database and creates the proper indexes to optimize the AdobePW method calls.

`./clean.sh adobepw.db`

* Download the latest version of the `blocks.json` file from the Recon-ng repository.

`curl -o blocks.json https://bitbucket.org/LaNMaSteR53/recon-ng/raw/master/data/adobe_blocks.json`

* Create an instance of the AdobePW class in a python interpreter.

``` python
import adobe
c = adobe.AdobePW('adobepw.db')
help(c)
```

* Import the `blocks.json` file.

`c.crack_hashes('blocks.json')`

* Search for hints associated with a target ciphertext (hash). The percent symbol (%) can be used as a wild card, similar to a SQL "LIKE" clause.

``` python
c.get_hints('<ciphertext>')
c.get_hints('<cipher>%')
c.get_hints('%<text>')
```

* Update the database with plaintext values.

`c.update_hash('<ciphertext>', '<plaintext>')`

* Create and email me your `blocks.json` file so that it can be merged with the master.

`c.create_block_db()`

## Sharing

Block files can be shared and merged. Merge acquired block files with a locally created block file before importing in order to identify conflicts between the block files.

`c.merge_block_dbs('your.json', 'my.json', 'merged.json')`

## FAQ

#### What guidelines do you have for claiming a hash as cracked? For example, I see a hint of "Adobe + one", so I assume the plaintext is "Adobe1", but that may be an incorrect  guess.
At this point, I am only interested in 99.9% confidence. There are plenty of very common blocks accompanied by large numbers of hints that haven't been cracked at this point. I would like to focus there for now.

#### How will you make sure that some jerk isn't trying to poison your data set by sending you bad block files?
I plan on manually validating submitted blocks until the process becomes unmanageable.

#### Is it safe to do this stuff on a VM?
No! Not unless you've given the VM some serious resources. In order to optimize analysis, some method calls have been configured for heavy memory use in exchange for speed.