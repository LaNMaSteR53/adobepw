echo "Vacuuming the database..."
sqlite3 -echo $1 "VACUUM;"

#echo "Archiving the database..."
#tar -zcvf $1.tar.gz $1

echo "Creating database indexes..."
sqlite3 -echo $1 "CREATE UNIQUE INDEX hash_lookup ON hashes (hash);"
sqlite3 -echo $1 "CREATE INDEX hint_lookup ON hints (hash);"
