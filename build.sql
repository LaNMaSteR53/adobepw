CREATE TABLE "hashes" (
    "hash" TEXT NOT NULL PRIMARY KEY,
    "count" INT NOT NULL,
    "password" TEXT NOT NULL
);
CREATE TABLE "hints" (
    "hash" TEXT NOT NULL REFERENCES "hashes" ("hash"),
    "username" TEXT NOT NULL,
    "hint" TEXT NOT NULL
);
--CREATE UNIQUE INDEX hash_lookup ON hashes (hash);
--CREATE INDEX hint_lookup ON hints (hash);
