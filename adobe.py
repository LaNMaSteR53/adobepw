from contextlib import closing
import json
import sqlite3
import subprocess
import sys

class AdobePW(object):
    """
    Tools for analyzing and manipulating the 2013 Adobe breach data.
    """

    def __init__(self, db_filename):
        """Initializes an instance of the AdobePW class.

        Keyword arguments:
        db_filename -- path to a database file containing the Adobe breach data
        """
        self.db_filename = db_filename
        self.array_size = 100000
        self.hashes_rows = 56045286

    ###################
    # PRIVATE METHODS #
    ###################

    def _build_cur(self, cur):
        """Builds an optimized cursor object."""
        # optimize sqlite for speed
        cur.execute('PRAGMA cache_size=10000')
        cur.execute('PRAGMA page_size=4096')
        cur.execute('PRAGMA temp_store=MEMORY')
        cur.execute('PRAGMA journal_mode=OFF')
        cur.execute('PRAGMA synchronous=OFF')
        return cur

    def _update_hashes(self, values):
        """Conducts bulk updates of plaintext values using executemany."""
        with sqlite3.connect(self.db_filename) as conn:
            with closing(conn.cursor()) as cur:
                cur = self._build_cur(cur)
                cur.executemany('UPDATE hashes SET password=? WHERE hash=?', values)
                conn.commit()

    def _result_iter(self, cursor, array_size=100000):
        """An iterator that uses fetchmany to optimize memory usage."""
        while True:
            results = cursor.fetchmany(array_size)
            if not results:
                break
            for result in results:
                yield result

    def _progress(self, cnt, tot):
        """Displays a progress bar."""
        percent = 100 * float(cnt) / float(tot)
        sys.stdout.write('[%-40s] %d%%   %d\r' % ('='*int(float(percent)/100*40), percent, cnt))
        sys.stdout.flush()

    ##################
    # PUBLIC METHODS #
    ##################

    def get_hints(self, search):
        """Searches the hashes in the database and displays hints associated with the results.

        Keyword arguments:
        search -- a hash or partial hash with SQL wildcards (%)
        """
        cmd = 'sqlite3 %s "SELECT hashes.*, hints.username, hints.hint FROM hashes JOIN hints ON hashes.hash=hints.hash WHERE hashes.hash LIKE \'%s\'"' % (self.db_filename, search)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in iter(p.stdout.readline,''):
            print line.strip()

    def update_hash(self, ciphertext, plaintext):
        """Updates the plaintext value of hash records in the database.

        Keyword arguments:
        ciphertext -- hash to update
        plaintext  -- plaintext value of the hash
        """
        values = [(plaintext, ciphertext)]
        self._update_hashes(values)

    def create_block_db(self, filename='blocks.json'):
        """Analyzes the hashes in the database and creates a JSON lookup table (block file) for all known cipher blocks.

        Keyword arguments:
        filename -- path to write the block file
        """
        print 'Creating block lookup table...'
        # declare local variables
        block_db = {}
        records = 0
        duplicate = 0
        unique = 0
        # process the database
        with sqlite3.connect(self.db_filename) as conn:
            with closing(conn.cursor()) as cur:
                cur.execute("SELECT hash, password FROM hashes")
                for pair in self._result_iter(cur, self.array_size):
                    if (records % self.array_size) == 0:
                        self._progress(records, self.hashes_rows)
                    records += 1
                    plain = pair[1]
                    # disregard uncracked hashes
                    if not plain: continue
                    cipher = pair[0]
                    # decode the hash into a string of hex, ciphertext
                    hexstr = ''.join([hex(ord(c))[2:].zfill(2) for c in cipher.decode('base64')])
                    # break up the ciphertext into 8 byte blocks
                    blocks = [hexstr[i:i+16] for i in range(0, len(hexstr), 16)]
                    pieces = [plain[i:i+8] for i in range(0, len(plain), 8)]
                    # account for blank block (e2a311ba09ab4707)
                    while len(pieces) < len(blocks):
                        pieces.append('')
                    # analyze each block
                    for i in range(0, len(blocks)):
                        # remove padding blocks
                        if pieces[i] == '*'*8: continue
                        if blocks[i] in block_db:
                            duplicate += 1
                            if block_db[blocks[i]] != pieces[i]:
                                print ''
                                print '+-[CONFLICT]'
                                print '|-%s' % (cipher)
                                print '|-%s' % (blocks[i])
                                print '|-OLD -> %s' % (block_db[blocks[i]])
                                print '|-NEW -> %s' % (pieces[i])
                        else:
                            block_db[blocks[i]] = pieces[i]
                            unique += 1
                self._progress(records, self.hashes_rows)
                print ''
        # write the block dictionary to a file
        with open(filename, 'w') as fp:
            json.dump(block_db, fp, indent=4)
        # statistical summary
        print "Processed {} unique blocks.".format(unique)
        print "Processed {} duplicate blocks.".format(duplicate)
        print "Resulted in {} blocks written to '{}'.".format(len(block_db), filename)

    def merge_block_dbs(self, dbin1, dbin2, dbout):
        """Merges two (2) block files.

        Keyword arguments:
        dbin1 -- first block file to merge
        dbin2 -- second block file to merge
        dbout -- path to write the merged block file
        """
        print 'Merging block files...'
        # create block lookup tables
        with open(dbin1) as fp:
            block_db1 = json.load(fp)
        with open(dbin2) as fp:
            block_db2 = json.load(fp)
        # compare block dbs
        for key in block_db1:
            if key in block_db2:
                if block_db1[key] != block_db2[key]:
                    print '+-[CONFLICT]'
                    print '|-%s' % (key)
                    print '|-%s -> %s' % (dbin1, block_db1[key])
                    print '|-%s -> %s' % (dbin2, block_db2[key])
                    print 'Merge failed.'
                    return
            else:
                block_db2[key] = block_db1[key]
                print 'New block. %s => %s' % (key, block_db2[key])
        # write the merged block dictionary to a file
        with open(dbout, 'w') as fp:
            json.dump(block_db2, fp, indent=4, sort_keys=True)
        print "Merge successful."

    def crack_hashes(self, filename):
        """Cracks the hashes in the database using known cipher blocks.

        Keyword arguments:
        filename -- path to a block file
        """
        print 'Cracking hashes...'
        # create block lookup table
        with open(filename) as fp:
            block_db = json.load(fp)
        # declare local variables
        values = []
        records = 0
        done = 0
        error = 0
        duplicate = 0
        new = 0
        skip = 0
        insert = 0
        # crack hashes
        with sqlite3.connect(self.db_filename) as conn:
            with closing(conn.cursor()) as cur:
                cur.execute("SELECT hash, password FROM hashes")
                for pair in self._result_iter(cur, self.array_size):
                    if (records % self.array_size) == 0:
                        self._progress(records, self.hashes_rows)
                    records += 1
                    plain = pair[1]
                    # disregard fully cracked hashes
                    if plain and '*'*8 not in plain:
                        done += 1
                        continue
                    cipher = pair[0]
                    # decode the hash into a string of hex, ciphertext
                    try:
                        hexstr = ''.join([hex(ord(c))[2:].zfill(2) for c in cipher.decode('base64')])
                    except Exception as e:
                        #print '%s => %s' % (cipher, str(e))
                        error += 1
                        continue
                    # break up the ciphertext into 8 byte blocks
                    blocks = [hexstr[i:i+16] for i in range(0, len(hexstr), 16)]
                    plaintext = ''
                    update = False
                    # reverse known cipher blocks
                    for block in blocks:
                        # check the block lookup table
                        if block in block_db:
                            plaintext += block_db[block]
                            update = True
                        # pad the plaintext for unknown blocks
                        else:
                            plaintext += '*'*8
                    # atleast a partial crack
                    if update:
                        # update the database
                        if plain == plaintext:
                            duplicate += 1
                        else:
                            values.append((plaintext, cipher))
                            new += 1
                            #print '%s => %s != %s' % (cipher, plaintext, plain)
                    else:
                        skip += 1
                self._progress(records, self.hashes_rows)
                print ''
        # store cracked hashes in the database
        print 'Writing cracked hashes to the database...'
        self._update_hashes(values)
        insert = len(values)
        # statistical summary
        print "Processed {} records.".format(records)
        print "Processed {} 'INSERT' statements.".format(insert)
        print "Ignored {} fully cracked hashes.".format(done)
        print "Cracked {} duplcate hashes.".format(duplicate)
        print "Cracked {} new hashes.".format(new)
        print "Unable to crack {} hashes.".format(skip)
        print "Encountered {} errors.".format(error)

    def get_statistics(self):
        """Analyzes the database and provides statistics about the data."""
        print 'Analyzing the database...'
        records = 0
        full = 0
        partial = 0
        padded = 0
        uncracked = 0
        with sqlite3.connect(self.db_filename) as conn:
            with closing(conn.cursor()) as cur:
                cur.execute("SELECT hash, password FROM hashes")
                for pair in self._result_iter(cur, self.array_size):
                    if (records % self.array_size) == 0:
                        self._progress(records, self.hashes_rows)
                    records += 1
                    plain = pair[1]
                    cipher = pair[0]
                    if plain and '*'*8 not in plain:
                        full += 1
                    elif plain and plain != len(plain) * '*':
                        partial +=1
                    elif plain and plain == len(plain) * '*':
                        padded += 1
                    elif not plain:
                        uncracked += 1
                self._progress(records, self.hashes_rows)
                print ''
        # statistical summary
        print "{} Total records.".format(records)
        print "{} Fully cracked hashes.".format(full)
        print "{} Partially cracked hashes.".format(partial)
        print "{} Fully padded hashes.".format(padded)
        print "{} Uncracked hashes.".format(uncracked)

if __name__ == "__main__":
    c = AdobePW(sys.argv[1])
    c.get_statistics()
    c.create_block_db('blocks.json')
    c.crack_hashes('blocks.json')
    c.get_statistics()
